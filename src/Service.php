<?php


namespace tensent\jwt;

use tensent\jwt\command\SecretCommand;
use tensent\jwt\middleware\InjectJwt;
use tensent\jwt\provider\JWT as JWTProvider;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(SecretCommand::class);
        $this->app->middleware->add(InjectJwt::class);
    }
}
