<?php


namespace tensent\jwt;

use tensent\jwt\claim\Factory;
use tensent\jwt\claim\Issuer;
use tensent\jwt\claim\Audience;
use tensent\jwt\claim\Expiration;
use tensent\jwt\claim\IssuedAt;
use tensent\jwt\claim\JwtId;
use tensent\jwt\claim\NotBefore;
use tensent\jwt\claim\Subject;

class Payload
{
    protected $factory;

    protected $classMap
        = [
            'aud' => Audience::class,
            'exp' => Expiration::class,
            'iat' => IssuedAt::class,
            'iss' => Issuer::class,
            'jti' => JwtId::class,
            'nbf' => NotBefore::class,
            'sub' => Subject::class,
        ];

    protected $claims;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function customer(array $claim = [])
    {
        foreach ($claim as $key => $value) {
            $this->factory->customer(
                $key,
                is_object($claim) && method_exists($claim, 'getValue') ? $value->getValue() : $value
            );
        }

        return $this;
    }

    public function get()
    {
        $claim = $this->factory->builder()->getClaims();

        return $claim;
    }

    public function check($refresh = false)
    {
        $this->factory->validate($refresh);

        return $this;
    }
}
