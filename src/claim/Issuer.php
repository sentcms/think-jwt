<?php


namespace tensent\jwt\claim;

class Issuer extends Claim
{
    protected $name = 'iss';
}
