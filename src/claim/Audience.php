<?php

namespace tensent\jwt\claim;

class Audience extends Claim
{
    protected $name = 'aud';
}
