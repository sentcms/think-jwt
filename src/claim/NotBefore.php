<?php


namespace tensent\jwt\claim;

class NotBefore extends Claim
{
    protected $name = 'nbf';
}
